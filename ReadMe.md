# FreeBitcoin
This is a python bot which automatically claims free roll on freebitco.in website. What it does is open a Chrome browser, login, check if free roll is available, solve captcha using anti-captcha, roll, and repeat.

---

## Python dependencies
### selenium - chrome browser automation
    pip install selenium
### Image - captcha image processing
    pip install Image
### requests - communication with captcha solving service
    pip install requests

---

## Program dependencies
* [Python 3](https://www.python.org/)
* [Google Chrome browser](https://www.google.com/chrome/browser/desktop/index.html)
* [chromedriver](https://sites.google.com/a/chromium.org/chromedriver/downloads)
* [geckodriver](https://github.com/mozilla/geckodriver/releases)

---

## How to run
### Step 1
create a config.json next to FreeBitcoin directory. Your config.json should be like this:

    {
        "email": ">>> Your freebitco.in login id <<<",
        "password": ">>> Your freebitco.in password <<<",
        "anti-captcha-api-key": ">>> Your anti-captcha API KEY <<<",
        "solve-recaptcha": true
    }

### Step 2
if you run on Windows, put chromedriver.exe and geckodriver.exe next to config.json

### Step 3
open an account on anti-captcha.com if you dont have

### Step 4
write your anti-captcha API key into config.json

### Step 5
if you decide not to solve google recaptcha, in config.json set "solve-recaptcha" as false

### Step 6
execute

    python ./FreeBitCoin/main.py

## Author
kmbear603@gmail.com

## Note
This is my first python project. Feel free to leave me comments.
