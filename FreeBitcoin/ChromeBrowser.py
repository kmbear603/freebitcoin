""" class file for ChromeBrowser class """

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from Browser import Browser

class ChromeBrowser(Browser):
    """ selenium chrome browser object """

    def _get_webdriver(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        return webdriver.Chrome(chrome_options=chrome_options)
