""" class file for captcha solver """

import abc

class CaptchaSolver(abc.ABC):
    """ base class for captcha solver """

    def __init__(self):
        pass

    @abc.abstractmethod
    def solve_image_to_text(self, base64_image):
        """ image to text captcha """
        return

    @abc.abstractmethod
    def solve_recaptcha(self, sitekey, url):
        """ google recaptcha """
        return
