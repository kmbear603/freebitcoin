""" class file of Rate """

import time
import requests

class Rate:
    """ Convert BTC to USD using coindesk API """

    def __init__(self):
        pass

    def get_USD(self):
        """ get BTC price in USD """
        return self._get_rate("USD")

    def _get_rate(self, symbol):
        """ get BTC price of symbol """
        response = requests.Session().get("https://api.coindesk.com/v1/bpi/currentprice.json").json()
        bpi = response.get("bpi")
        return bpi.get(symbol).get("rate_float");
