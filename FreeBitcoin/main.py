""" driver program """

from Console import Console
from Bot import Bot

def main():
    """ driver function """
    Console.clear()
    Console.log_with_timestamp("start")

    bot = Bot()
    bot.run()

if __name__ == "__main__":
    main()
