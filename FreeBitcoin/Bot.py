""" class file for Bot """

import time
from ChromeBrowser import ChromeBrowser
from FirefoxBrowser import FirefoxBrowser
from Configuration import Configuration
from Console import Console
from AntiCaptcha import AntiCaptcha
from Rate import Rate

class Bot:
    """ main engine """

    hostName = "https://freebitco.in/"

    def __init__(self):
        self.browser = None

    def _show_status(self, status):
        """ show status to console """
        Console.log_with_timestamp(status)

    def run(self):
        """
        main loop to check whether it is available to roll,
        and if it is available, just roll it
        """

        fmt = "{0:.8f}"

        config = Configuration("config.json")

        Console.log_with_timestamp("initializing captcha solver")
        captcha = AntiCaptcha(config.anti_captcha_api_key)
        captcha_balance = captcha.get_balance()
        self._show_status("Current balance on anti-captcha: " + fmt.format(captcha_balance))
        if captcha_balance < 2:
            self._show_status("balance on captcha solver is too low: " + captcha_balance)
            return

        starting_balance = None
        starting_captcha_balance = captcha_balance

        recaptcha_count = 0
        image_captcha_count = 0
        show_wait_msg = True

        while True:
            self._show_status("create browser")
            self._create_browser()

            self._show_status("login")
            while not self._login(config.email, config.password):
                self._show_status("login failed, try again after 1 minute")
                time.sleep(60)

            self._show_status("wait 3 seconds to finish login")
            time.sleep(3)

            balance = self._get_balance()
            self._show_status("current balance on FreeBitcoin: "\
                + fmt.format(balance)\
                + ", in USD=" + fmt.format(self._get_BTC_price_in_USD(balance)))

            if starting_balance is None:
                starting_balance = balance

            start_loop = time.time()
            while time.time() < start_loop + (24 * 60 * 60):  # force restart browser after 1 day
                try:
                    if self._is_available_to_roll():
                        self._show_status("free roll is available")

                        captcha_count = 0
                        if self._is_recaptcha():
                            captcha_count += 1
                        if self._is_image_captcha():
                            captcha_count += 1
                        if self._is_image_captcha2():
                            captcha_count += 1

                        if captcha_count > 1:
                            self._show_status("multiple captcha found, skip solving")
                            self._show_status("sleep 15 minutes")
                            time.sleep(15 * 60)
                            solved = False
                        elif self._is_recaptcha():
                            if config.solve_repatcha:
                                self._show_status("solving recaptcha")
                                solved, cost = self._solve_recaptcha(captcha)
                            else:
                                self._show_status("skip solving recaptcha according to config")
                                self._show_status("sleep 15 minutes")
                                time.sleep(15 * 60)
                                solved = False
                            if solved:
                                recaptcha_count += 1
                        elif self._is_image_captcha():
                            self._show_status("solving image captcha")
                            solved, cost = self._solve_image_captcha(captcha)
                            if solved:
                                image_captcha_count += 1
                        elif self._is_image_captcha2():
                            self._show_status("solving new image captcha 2")
                            solved, cost = self._solve_image_captcha2(captcha)
                            if solved:
                                image_captcha_count += 1
                        elif self._is_image_captcha3():
                            self._show_status("solving image captcha 3")
                            solved, cost = self._solve_image_captcha3(captcha)
                            if solved:
                                image_captcha_count += 1
                        else:
                            self._show_status("unknown captcha")
                            self._show_status("sleep 15 minutes")
                            time.sleep(15 * 60)
                            solved = False

                        if solved:
                            self._show_status("roll")
                            roll_success = self._roll()

                            if not roll_success:
                                self._show_status("failed in rolling, probably caused by wrong captcha")
                                self._show_status("try again after 1 minute")
                                time.sleep(60)
                            else:
                                ref_balance = self._get_balance()

                                trial = 1
                                while trial <= 10 and ref_balance == balance:
                                    time.sleep(1)
                                    trial += 1
                                    ref_balance = self._get_balance()

                                # wait until the balance element on webpage is not updated anymore
                                new_balance = self._get_balance()
                                trial = 1
                                while trial <= 10 and new_balance != ref_balance:
                                    time.sleep(1)
                                    trial += 1
                                    new_balance = self._get_balance()

                                gain_in_this_roll = new_balance - balance
                                cumulated_gain = new_balance - starting_balance
                                self._show_status("FreeBitcoin: balance=" + fmt.format(new_balance) \
                                        + ", +" + fmt.format(gain_in_this_roll) \
                                        + ", since start=+" + fmt.format(cumulated_gain) \
                                        + ", in USD=" + fmt.format(self._get_BTC_price_in_USD(new_balance)))

                                new_captcha_balance = captcha.get_balance()
                                trial = 1
                                while trial <= 5 and new_captcha_balance == captcha_balance:
                                    time.sleep(30)
                                    trial += 1
                                    new_captcha_balance = captcha.get_balance()
                                cost_in_this_roll = cost
                                cumulated_cost = starting_captcha_balance - new_captcha_balance

                                self._show_status("anti-captcha: balance=" \
                                        + fmt.format(new_captcha_balance) \
                                        + ", -" + fmt.format(cost_in_this_roll) \
                                        + ", since start=-" + fmt.format(cumulated_cost))

                                self._show_status("captcha statistics: recaptcha=" + str(recaptcha_count) \
                                        + ", image=" + str(image_captcha_count))

                                balance = new_balance
                                captcha_balance = new_captcha_balance

                                self._show_status("wait for free roll")
                                time.sleep(55 * 60)
                        else:
                            self._show_status("failed to solve captcha")
                            self.browser.refresh()
                            show_wait_msg = True
                    else:
                        if show_wait_msg:
                            self._show_status("wait for free roll")
                            show_wait_msg = False
                except Exception as e:
                    self._show_status("exception happened")
                    self._show_status(e.__doc__)
                    #self._show_status(e.message)
                time.sleep(60)

            self._destroy_browser()

    def _get_BTC_price_in_USD(self, btc):
        """ get price in USD """
        return btc * Rate().get_USD()

    def _destroy_browser(self):
        """ destroy the browser object """
        self.browser.close()
        self.browser = None

    def _create_browser(self):
        """ create the browser object """
        self.browser = ChromeBrowser()
        self.browser.open()

        # use narrow window to force it to render in mobile mode
        self.browser.set_window_position(0, 0)
        self.browser.set_window_size(400, 600)

    def _login(self, email, password):
        """ login user account """
        self.browser.go_to(self.hostName)
        self.browser.click("//*[@id=\"homepage_login_button\"]/button")
        self.browser.input_text("//*[@id=\"login_form_btc_address\"]", email)
        self.browser.input_text("//*[@id=\"login_form_password\"]", password)
        self.browser.scroll_into_view("//*[@id=\"login_button\"]")
        self.browser.click("//*[@id=\"login_button\"]")
        while True:
            if self.browser.is_visible("//*[contains(@class, 'reward_point_redeem_result_error')]"):
                return False
            if self.browser.is_exist("//*[@id=\"free_play_form_button\"]"):
                return True
            time.sleep(1)

    def _get_balance(self):
        """ get current balance """
        txt = self.browser.get_text("//*[@id=\"balance2\"]")
        try:
            space = txt.index(' ')
        except ValueError:
            space = -1
        if space != -1:
            txt = txt[:space]
        return float(txt)

    def _is_available_to_roll(self):
        """ check if it is available to roll now """
        return self.browser.is_visible("//*[@id=\"free_play_form_button\"]")

    def _is_recaptcha(self):
        """ check if the verification is google recaptha or proprietary method """
        return self.browser.is_visible("//*[@id=\"free_play_recaptcha\"]")

    def _solve_recaptcha(self, solver):
        """ solve repatcha """
        """
        return False
        """
        sitekey = self.browser.get_attribute_of_element(\
                        "//div[@class='g-recaptcha']", "data-sitekey")
        solution, cost = solver.solve_recaptcha(sitekey, self.hostName)
        if solution is None:
            return False, 0

        self.browser.set_value("//textarea[@id='g-recaptcha-response']", solution)
        return True, cost

    def _is_image_captcha_core(self, id):
        """ check if captcha is image captcha """
        return self.browser.is_visible("//*[@id=\"" + id + "\"]/div[1]/img")

    def _solve_image_captcha_core(self, id, solver):
        """ solve image captcha """
        base64_captcha = self.browser.get_base64_image(\
                "//*[@id=\"" + id + "\"]/div[1]/img")
        solution, cost = solver.solve_image_to_text(base64_captcha)
        if solution is None:
            return False, 0
        self.browser.set_value("//*[@id=\"" + id + "\"]/input[2]", solution)
        return True, cost

    def _is_image_captcha(self):
        """ check if captcha is image captcha """
        return self._is_image_captcha_core("captchasnet_free_play_captcha")

    def _solve_image_captcha(self, solver):
        """ solve image captcha """
        return self._solve_image_captcha_core("captchasnet_free_play_captcha", solver)

    def _is_image_captcha2(self):
        """ check if captcha is new image captcha """
        return self._is_image_captcha_core("securimage_free_play_captcha")

    def _solve_image_captcha2(self, solver):
        """ solve new image captcha """
        return self._solve_image_captcha_core("securimage_free_play_captcha", solver)

    def _is_image_captcha3(self):
        """ check if captcha is new image captcha """
        return self._is_image_captcha_core("botdetect_free_play_captcha")

    def _solve_image_captcha3(self, solver):
        """ solve new image captcha """
        return self._solve_image_captcha_core("botdetect_free_play_captcha", solver)

    def _roll(self):
        """ click roll """
        self.browser.click("//*[@id=\"free_play_form_button\"]")
        time.sleep(3)
        trial = 1
        while trial <= 10 and self._is_available_to_roll():
            trial += 1
            time.sleep(1)
        return not self._is_available_to_roll()
