""" class file for anti-captcha """

import time
import requests
from CaptchaSolver import CaptchaSolver
from Console import Console

class AntiCaptcha(CaptchaSolver):
    """ helper class for anti-captcha.com """

    API_TIMEOUT = 10
    API_ENDPOINT_CREATETASK = "https://api.anti-captcha.com/createTask"
    API_ENDPOINT_CHECKBALANCE = "https://api.anti-captcha.com/getBalance"
    API_ENDPOINT_GETRESULT = "https://api.anti-captcha.com/getTaskResult"

    def __init__(self, api_key):
        super().__init__()
        self.api_key = api_key

    def get_balance(self):
        """ get remaining balance on anti-captcha """
        response = self._post(self.API_ENDPOINT_CHECKBALANCE, {
            "clientKey": self.api_key
        })
        if response is None:
            return None
        return response["balance"]

    @classmethod
    def _post(cls, url, json_data):
        """ make a post request to anti-captcha """
        headers = {
            "Content-Type": "application/json",
            "Acccept": "application/json"
        }

        response = requests.Session().post(url, headers=headers, json=json_data).json()

        if response.get('errorId', False):
            Console.error_with_timestamp(\
                url\
                + " " + str(response['errorId'])\
                + " " + response['errorCode']\
                + " " + response['errorDescription'])
            return None

        return response

    def solve_image_to_text(self, base64_image):
        response = self._post(self.API_ENDPOINT_CREATETASK, {
            "clientKey": self.api_key,
            "task": {
                "type": "ImageToTextTask",
                "body": base64_image
            }
        })

        if response is None:
            return None

        solution, cost = self._get_task_result(response["taskId"])
        if solution is None:
            return None

        return solution["text"], cost

    def solve_recaptcha(self, sitekey, url):
        response = self._post(self.API_ENDPOINT_CREATETASK, {
            "clientKey": self.api_key,
            "task": {
                "type": "NoCaptchaTaskProxyless",
                "websiteURL": url,
                "websiteKey": sitekey
            }
        })

        if response is None:
            return None

        solution, cost = self._get_task_result(response["taskId"])
        if solution is None:
            return None

        return solution["gRecaptchaResponse"], cost

    def _get_task_result(self, task_id):
        """ get captcha result """

        while True:
            response = self._post(self.API_ENDPOINT_GETRESULT, {
                "clientKey": self.api_key,
                "taskId": task_id
            })

            if response["status"] == "processing":
                time.sleep(5)
                continue
            elif response["status"] == "ready":
                return response["solution"], float(response["cost"])
            else:
                Console.error_with_timestamp("unknown status in response: " + response["status"])
                return None
