""" Console class file """

import os
import datetime

class Console:
    """ Console class """

    @classmethod
    def clear(cls):
        """ clear console """
        os.system('cls')

    @classmethod
    def log(cls, msg):
        """ write msg to console """
        print(msg)

    @classmethod
    def error(cls, msg):
        """ write error to console """
        cls.log(msg)

    @classmethod
    def log_with_timestamp(cls, msg):
        """ write msg to console with timestamp heading """
        cls.log("{0:%Y-%m-%d %H:%M:%S} {1}".format(datetime.datetime.now(), msg))

    @classmethod
    def error_with_timestamp(cls, msg):
        """ write error to console with timestamp heading """
        cls.log_with_timestamp(msg)
        