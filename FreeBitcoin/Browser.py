""" class file for Browser class """

import abc
import math
import base64
import io
import time
from PIL import Image
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException

class Browser(abc.ABC):
    """ base class for all selenium webdriver wrappers """

    def __init__(self):
        self.driver = None

    @abc.abstractmethod
    def _get_webdriver(self) -> webdriver:
        """ create the webdriver instance """
        return

    def open(self):
        """ open browser """
        self.driver = self._get_webdriver()

    def close(self):
        """ close browser """
        if self.driver is not None:
            self.driver.quit()
            self.driver = None

    def get_window_position(self):
        """ get window position """
        return self.driver.get_window_position()

    def set_window_position(self, pos_x, pos_y):
        """ set window position """
        self.driver.set_window_position(pos_x, pos_y)

    def get_window_size(self):
        """ get window size """
        return self.driver.get_window_size()

    def set_window_size(self, width, height):
        """ set window size """
        self.driver.set_window_size(width, height)

    def go_to(self, url):
        """ navigate browser to specual url """
        self.driver.get(url)

    def refresh(self):
        """ refresh page """
        self.driver.refresh()

    def click(self, xpath):
        """ click button """
        element = self.driver.find_element_by_xpath(xpath)
        #actions = ActionChains(self.driver)
        #actions.move_to_element(element).perform()
        self.scroll_into_view(xpath)
        self.driver.execute_script("arguments[0].click();", element)
        #element.click()

    def get_text(self, xpath):
        """ get text of element """
        element = self.driver.find_element_by_xpath(xpath)
        return element.get_attribute("innerText")

    def input_text(self, xpath, text):
        """ input text to the edit box specified by xpath """
        element = self.driver.find_element_by_xpath(xpath)
        element.clear()
        element.send_keys(text)

    def set_value(self, xpath, text):
        """ input text to the edit box specified by xpath """
        element = self.driver.find_element_by_xpath(xpath)
        self.driver.execute_script("arguments[0].value=\"" + text + "\";", element)

    def get_base64_image(self, xpath):
        """ get a base64 string of the specificed img """

        # get pixel ratio first
        ratio = self.driver.execute_script("return window.devicePixelRatio;")

        element = self.driver.find_element_by_xpath(xpath)

        # scroll to bottom to see the captcha image
        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        time.sleep(1)   # wait 1s for it to scroll

        scrolled_y = self.driver.execute_script("return window.pageYOffset;")

        src_base64 = self.driver.get_screenshot_as_base64()
        scr_png = base64.b64decode(src_base64)
        scr_img = Image.open(io.BytesIO(scr_png))
        #scr_img.show();

        _x = math.floor(element.location["x"] * ratio)
        _y = math.floor((element.location["y"] - scrolled_y) * ratio)
        _w = math.ceil(element.size["width"] * ratio)
        _h = math.ceil(element.size["height"] * ratio)

        captcha_img = scr_img.crop((_x, _y, _x + _w, _y + _h))
        #captcha_img.show()

        temp_filename = "captcha.png"
        captcha_img.save(temp_filename)
        base64_text = base64.b64encode(open(temp_filename, "rb").read()).decode("utf-8")

        return base64_text

    def is_exist(self, xpath):
        """ check if a web element exists in the dom """
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True

    def is_visible(self, xpath):
        """ check if a webelement is visible on screen """
        try:
            element = self.driver.find_element_by_xpath(xpath)
            return element.is_displayed()
        except NoSuchElementException:
            return False

    def switch_to_document(self):
        """ switch context to default frame """
        self.driver.switch_to.default_content()

    def switch_to_iframe(self, xpath):
        """ switch context to iframe """
        iframe_element = self.driver.find_element_by_xpath(xpath)
        self.driver.switch_to.frame(iframe_element)

    def get_attribute_of_element(self, xpath, attr_name):
        """ get attribute value """
        element = self.driver.find_element_by_xpath(xpath)
        return element.get_attribute(attr_name)

    def scroll_into_view(self, xpath):
        """ scroll into view """
        element = self.driver.find_element_by_xpath(xpath)
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
