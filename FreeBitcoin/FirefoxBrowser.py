""" class file for FirefoxBrowser class """

from selenium import webdriver
from Browser import Browser

class FirefoxBrowser(Browser):
    """ selenium chrome browser object """

    def _get_webdriver(self):
        return webdriver.Firefox()
