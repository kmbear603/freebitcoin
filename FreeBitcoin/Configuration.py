""" class file for configuration json """

import json

class Configuration:
    """ class defintion for configuraton json """

    def __init__(self, file_name):
        data = json.load(open(file_name))
        self.email = data["email"]
        self.password = data["password"]
        self.anti_captcha_api_key = data["anti-captcha-api-key"]
        self.solve_repatcha = data["solve-recaptcha"]
